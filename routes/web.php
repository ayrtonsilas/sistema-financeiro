<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'HomeController@index',
]);

Route::get('/didaticos/{materia}', 'HomeController@didaticos');
Route::get('/paradidaticos', 'HomeController@paradidaticos');

Auth::routes();


/*recebimentos*/
Route::prefix('recebimentos')->group(function () {
    Route::get('/', [
        'uses' => 'RecebimentoController@listar',
    ]);
    Route::get('/novo', [
        'uses' => 'RecebimentoController@novo',
    ]);
    Route::get('/editar/{recebimento}', [
        'uses' => 'RecebimentoController@editar',
    ]);
    Route::post('/salvar', [
        'uses' => 'RecebimentoController@salvar',
    ]);
    Route::delete('/excluir/{recebimento}', [
        'uses' => 'RecebimentoController@excluir',
    ]);
});

/*recebimentos*/
Route::prefix('parcela')->group(function () {
    Route::get('/{tela}/{acao}/{id}', [
        'uses' => 'ParcelaController@parcela',
    ]);
    Route::post('/salvar', [
        'uses' => 'ParcelaController@salvar',
    ]);
    Route::post('/excluir', [
        'uses' => 'ParcelaController@excluir',
    ]);
});


/*pagamentos*/
Route::prefix('pagamentos')->group(function () {
    Route::get('/', [
        'uses' => 'PagamentoController@listar',
    ]);
    Route::get('/novo', [
        'uses' => 'PagamentoController@novo',
    ]);
    Route::get('/editar/{pagamento}', [
        'uses' => 'PagamentoController@editar',
    ]);
    Route::post('/salvar', [
        'uses' => 'PagamentoController@salvar',
    ]);
    Route::delete('/excluir/{pagamento}', [
        'uses' => 'PagamentoController@excluir',
    ]);
});
