<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ViewRecebimento extends Model
{
    protected $table = 'viewrecebimentosdados';
    protected $primaryKey = 'id';

    public $timestamps = false;
    public $incrementing = false;
    
}
