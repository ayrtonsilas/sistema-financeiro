<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Pagamento extends Model
{
    protected $table = 'pagamento';
    protected $primaryKey = 'id';
    protected $fillable = [
        'fornecedor_id', 'tipo_id', 'data_inclusao','observacoes', 'operador_id'
    ];
    public $timestamps = false;
    

    public static function tipos(){
       return collect(\DB::select('select * from tipo_rec_pag'));
    }
    public static function fornecedores(){
        return collect(\DB::select('select * from fornecedor'));
    }
    public static function operadores(){
        return collect(\DB::select('select * from operador'));
    }

}
