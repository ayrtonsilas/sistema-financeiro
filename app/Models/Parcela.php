<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Parcela extends Model
{
    protected $table = 'parcela';
    protected $primaryKey = 'id';
    protected $fillable = [
        'valor', 'valor_quitado', 'data_vencimento', 'data_quitacao'
    ];
    public $timestamps = false;

}
