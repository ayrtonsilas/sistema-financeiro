<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ViewPagamento extends Model
{
    protected $table = 'viewpagamentosdados';
    protected $primaryKey = 'id';

    public $timestamps = false;
    public $incrementing = false;
    
}
