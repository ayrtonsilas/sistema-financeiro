<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Recebimento extends Model
{
    protected $table = 'recebimento';
    protected $primaryKey = 'id';
    protected $fillable = [
        'cliente_id', 'tipo_id', 'desconto_id', 'data_inclusao','observacoes', 'operador_id'
    ];
    public $timestamps = false;
    

    public static function tipos(){
       return collect(\DB::select('select * from tipo_rec_pag'));
    }
    public static function clientes(){
        return collect(\DB::select('select * from cliente'));
    }
    public static function descontos(){
        return collect(\DB::select('select * from desconto'));
    }
    public static function operadores(){
        return collect(\DB::select('select * from operador'));
    }

}
