<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Livro;
use Illuminate\Support\Facades\{DB, Input};

class LivroController extends Controller
{
    //mostra os livros
    public function listar(){
        $livros = Livro::where('id_usuario','=',Auth::user()->id)->get();

        return view('livros.listar',
        [
            'livros' => $livros,
            'mensagem' => [],
        ]);
    }
    //monta um novo livro
    public function novo(){
        $livro = New Livro();
        $livro->id = 0;
        return self::editar($livro);
    }
    //editar um livro parassando o id do livro por parametro e o laravel ja converte pra objeto
    public function editar(Livro $livro){
        if(empty($livro->id)){
            $acao = 'novo';
        }else{
            $acao = 'editar';
        }

        return view('livros.form',
        [
            'livro' => $livro,
            'acao'  => $acao
        ]);
    }

    public function excluir(Livro $livro){
        try{
            $livro->delete();
            return redirect()->action('LivroController@listar')
                ->with('success', 'Livro Excluido com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('LivroController@listar')
                ->with('error', 'Não foi possível excluir')
                ->withInput(Input::all());
        }
    }
    public function salvar(Request $request){
        try{
            //busca um livro existente ou instancia caso o id sera zero
            $livro = Livro::findOrNew($request->id);
            //pega todos os elementos vindo da requisição e ja atribui na variavel livro
            $livro->fill($request->all());
            //pega o id do usuario logado
            $livro->id_usuario = Auth::user()->id;
            if ($livro->materia === "")
                $livro->materia = null;
            $livro->save();
            return redirect()->action('LivroController@listar')
                ->with('success', 'Livro Salvo com sucesso!');
             

        }catch(\Exception $e){
            return redirect()->action('LivroController@listar')
                ->with('error', 'Não foi possível salvar')
                ->withInput(Input::all());
        }

    }
}