<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Pagamento,ViewPagamento};
use Illuminate\Support\Facades\{DB, Input};

class PagamentoController extends Controller
{

    public function listar(){
        $pagamentos = ViewPagamento::orderBy('ID')->get();
        return view('pagamentos.lista',
        [
            'pagamentos' => $pagamentos,
            'mensagem' => [],
        ]);
    }

    public function novo(){
        $pagamento = New Pagamento();
        $pagamento->id = 0;
        $pagamento->data_inclusao = date('Y-m-d h:i:s');
        return self::editar($pagamento);
    }

    public function editar(Pagamento $pagamento){
        $parcelas = collect([]);
        if(empty($pagamento->id)){
            $acao = 'novo';
        }else{
            $acao = 'editar';
            $parcelas = collect(\DB::select('select * from viewvalorespagamentos where pagamento_id = ?',[$pagamento->id]));
        }

        return view('pagamentos.form',
        [
            'pagamento' => $pagamento,
            'acao'  => $acao,
            'tipos' => Pagamento::tipos(),
            'fornecedores' => Pagamento::fornecedores(),
            'operadores' => Pagamento::operadores(),
            'parcelas' => $parcelas
        ]);
    }

    public function excluir(Pagamento $pagamento){
        try{
            $pagamento->delete();
            return redirect()->action('PagamentoController@listar')
                ->with('success', 'Pagamento Excluido com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('PagamentoController@listar')
                ->with('error', 'Não foi possível excluir')
                ->withInput(Input::all());
        }
    }
    public function salvar(Request $request){
        try{
            $pagamento = Pagamento::findOrNew($request->id);
            $pagamento->fill($request->all());
            $pagamento->save();

            return redirect()->action('PagamentoController@listar')
                ->with('success', 'Pagamento Salvo com sucesso!');
             

        }catch(\Exception $e){
            dd($e->getMessage());
            return redirect()->action('PagamentoController@listar')
                ->with('error', 'Não foi possível salvar')
                ->withInput(Input::all());
        }

    }
}