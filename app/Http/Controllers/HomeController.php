<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Anuncio;
use App\Models\Livro;
use App\Models\Interesse;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //public function __construct()
    //{
    //    $this->middleware('auth');
    //}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function pesquisar(Request $req){
        $anuncios = Anuncio::join('livros','livros.id','=','anuncios.id_livro');
        $busca = trim($req->buscar);

        if(!empty($busca)){
            $anuncios = $anuncios
            ->where(function($q) use ($busca){
                $q->where('livros.autor','like','%'.$busca.'%')
                  ->orWhere('livros.materia','like','%'.$busca.'%')
                  ->orWhere('livros.titulo','like','%'.$busca.'%')
                  ->orWhere('anuncios.nome','like','%'.$busca.'%');
            });
        }
        $anuncios = $anuncios->select('anuncios.*','livros.autor', 'livros.materia', 'livros.edicao','livros.titulo')->get();
        return view('home',['anuncios' => $anuncios]);
    }
    public function detalhes ($idAnuncio){
        $anuncio = Anuncio::find($idAnuncio);
        return view('detalhes', compact('anuncio'));
    }
    
    public function didaticos($materia)
    {
        $anuncios = Anuncio::tipo($materia);
        return view('home', compact('anuncios'));
    }
    
    public function paradidaticos()
    {
        $anuncios = Anuncio::tipo();
        return view('home', compact('anuncios'));
    }
}
