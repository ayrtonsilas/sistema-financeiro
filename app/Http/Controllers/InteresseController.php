<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Interesse;
use Auth;

class InteresseController extends Controller
{
    public function salvar(Request $request){
        try{
            $interesse = New Interesse();
            $interesse->fill($request->all());
            $interesse->save();
            return redirect()->action('HomeController@index')
                ->with('success', 'Interesse cadastrado com sucesso!');
             

        }catch(\Exception $e){
            dd($e);
            return redirect()->action('HomeController@index')
                ->with('error', 'Não foi possível salvar!');
        }
    }
    public function listar(){
        $interesses = Interesse::getInteressesUser();
        return View('interesses.listar',['interesses' => $interesses]);
    }
}
