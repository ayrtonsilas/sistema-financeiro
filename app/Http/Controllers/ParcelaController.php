<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Recebimento,ViewRecebimento,Parcela,Pagamento};
use Illuminate\Support\Facades\{DB, Input};

class ParcelaController extends Controller
{

    public function parcela($tela,$acao,$id){
        if($acao == 'novo'){
            if($tela == 'r'){
                $parcela = New Parcela(['id' => 0]);
                $parcela->pai = $id;
                $parcela->pailocal = 'r';
            }else{
                $parcela = New Parcela(['id' => 0]);
                $parcela->pai = $id;
                $parcela->pailocal = 'p';
            }
        }else if($acao == 'editar'){
            if($tela == 'r'){
                $parcela = collect(\DB::select('select * from viewvaloresrecebimento where parcela_id = ?',[$id]))->first();
                $parcela->pai = $parcela->recebimento_id;
                $parcela->pailocal = 'r';
            }else{
                $parcela = collect(\DB::select('select * from viewvalorespagamentos where parcela_id = ?',[$id]))->first();
                $parcela->pai = $parcela->pagamento_id;
                $parcela->pailocal = 'p';
            }
        }

        return view('parcela',['parcela' => $parcela]);
    }

    public function excluir(Request $req){
        try{
            $local = $req->pailocal;
            $pai = $req->pai;
            
            $deletou = false;
            if($local == 'r'){
                DB::table('assoc_recebimento_parcela')->where('parcela_id', '=', $req->id)->delete();
                $deletou = true;
            }else if($local == 'p'){
                DB::table('assoc_pagamento_parcela')->where('parcela_id', '=', $req->id)->delete();
                $deletou = true;
            }

            if($deletou){
                Parcela::where('id',$req->id)->delete();
            }

            if($local == 'r'){
                return redirect()->action('RecebimentoController@editar',['recebimento' => Recebimento::find($pai)]);
            }else if($local == 'p'){
                return redirect()->action('PagamentoController@editar',['pagamento' => Pagamento::find($pai)]);
            }
             

        }catch(\Exception $e){
            dd($e->getMessage());
            return redirect()->action('RecebimentoController@listar')
                ->with('error', 'Não foi possível salvar')
                ->withInput(Input::all());
        }
    }
    public function salvar(Request $req){
        try{
            $local = $req->pailocal;
            $pai = $req->pai;
            $parcela = Parcela::findOrNew($req->id);
            $parcela->fill($req->all());
            $parcela->save();

            if($req->id > 0){
                
            }else{
                if($local == 'r'){
                    $ultimaParcela = Parcela::orderBy('id','desc')->first();
                    \DB::table('assoc_recebimento_parcela')->insert(
                        ['recebimento_id' => $pai, 'parcela_id' => $ultimaParcela->id]
                    );
                }else if($local == 'p'){
                    $ultimaParcela = Parcela::orderBy('id','desc')->first();
                    \DB::table('assoc_pagamento_parcela')->insert(
                        ['pagamento_id' => $pai, 'parcela_id' => $ultimaParcela->id]
                    );
                }
            }

            if($local == 'r'){
                return redirect()->action('RecebimentoController@editar',['recebimento' => Recebimento::find($pai)]);
            }else if($local == 'p'){
                return redirect()->action('PagamentoController@editar',['pagamento' => Pagamento::find($pai)]);
            }
             

        }catch(\Exception $e){
            return redirect()->action('RecebimentoController@listar')
                ->with('error', 'Não foi possível salvar')
                ->withInput(Input::all());
        }

    }
}