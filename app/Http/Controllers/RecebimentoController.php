<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Recebimento,ViewRecebimento};
use Illuminate\Support\Facades\{DB, Input};

class RecebimentoController extends Controller
{

    public function listar(){
        $recebimentos = ViewRecebimento::orderBy('ID')->get();
        return view('recebimentos.lista',
        [
            'recebimentos' => $recebimentos,
            'mensagem' => [],
        ]);
    }

    public function novo(){
        $recebimento = New Recebimento();
        $recebimento->id = 0;
        $recebimento->data_inclusao = date('Y-m-d h:i:s');
        return self::editar($recebimento);
    }

    public static function editar(Recebimento $recebimento){
        $parcelas = collect([]);
        if(empty($recebimento->id)){
            $acao = 'novo';
        }else{
            $acao = 'editar';
            $parcelas = collect(\DB::select('select * from viewvaloresrecebimento where recebimento_id = ?',[$recebimento->id]));
        }

        return view('recebimentos.form',
        [
            'recebimento' => $recebimento,
            'acao'  => $acao,
            'tipos' => Recebimento::tipos(),
            'clientes' => Recebimento::clientes(),
            'descontos' => Recebimento::descontos(),
            'operadores' => Recebimento::operadores(),
            'parcelas' => $parcelas
        ]);
    }

    public function excluir(Recebimento $recebimento){
        try{
            $recebimento->delete();
            return redirect()->action('RecebimentoController@listar')
                ->with('success', 'Recebimento Excluido com sucesso!');
        }catch(\Exception $e){
            return redirect()->action('RecebimentoController@listar')
                ->with('error', 'Não foi possível excluir')
                ->withInput(Input::all());
        }
    }
    public function salvar(Request $request){
        try{
            $recebimento = Recebimento::findOrNew($request->id);
            $recebimento->fill($request->all());
            $recebimento->save();

            return redirect()->action('RecebimentoController@listar')
                ->with('success', 'Recebimento Salvo com sucesso!');
             

        }catch(\Exception $e){
            dd($e->getMessage());
            return redirect()->action('RecebimentoController@listar')
                ->with('error', 'Não foi possível salvar')
                ->withInput(Input::all());
        }

    }
}