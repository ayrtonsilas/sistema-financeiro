<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Pedro Augusto',
                'email' => 'pedroaugustovitor@hotmail.com',
                'password' => '$2y$10$NryUMdH7ZkHjBFsDYOnJ0u8c/bYMETcYI08u3.6F6kcna059bvRvS',
                'data_nasc' => '1119-11-11',
                'telefone' => '7191919191',
                'remember_token' => NULL,
                'created_at' => '2018-07-10 15:20:55',
                'updated_at' => '2018-07-10 15:20:55',
            ),
        ));
        
        
    }
}