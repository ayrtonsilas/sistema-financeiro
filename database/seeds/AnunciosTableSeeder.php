<?php

use Illuminate\Database\Seeder;

class AnunciosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('anuncios')->delete();
        
        \DB::table('anuncios')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_usuario' => 1,
                'id_livro' => 1,
                'nome' => 'Baratinho',
                'preco' => 123.0,
                'estado' => 'Novo',
                'imagem' => NULL,
                'descricao' => '123123',
                'created_at' => '2018-07-10 15:21:42',
                'updated_at' => '2018-07-11 14:32:22',
            ),
        ));
        
        
    }
}