<?php

use Illuminate\Database\Seeder;

class LivrosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('livros')->delete();
        
        \DB::table('livros')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_usuario' => 1,
                'titulo' => 'Alice no País das Maravilhas',
                'autor' => 'Rabbit',
                'materia' => NULL,
                'edicao' => '1',
                'tipo' => 'paradidatico',
                'created_at' => '2018-07-10 15:21:21',
                'updated_at' => '2018-07-10 15:21:21',
            ),
            1 => 
            array (
                'id' => 2,
                'id_usuario' => 1,
                'titulo' => 'Sapiens - Uma Breve História da Humanidade',
                'autor' => 'Yuval Noah Harari',
                'materia' => NULL,
                'edicao' => '1',
                'tipo' => 'paradidatico',
                'created_at' => '2018-07-11 14:34:00',
                'updated_at' => '2018-07-11 14:34:00',
            ),
            2 => 
            array (
                'id' => 3,
                'id_usuario' => 1,
                'titulo' => 'O Poder do Hábito',
                'autor' => 'Charles Duhigg',
                'materia' => NULL,
                'edicao' => '2',
                'tipo' => 'paradidatico',
                'created_at' => '2018-07-11 14:34:22',
                'updated_at' => '2018-07-11 14:34:22',
            ),
            3 => 
            array (
                'id' => 4,
                'id_usuario' => 1,
                'titulo' => 'Fahrenheit 451',
                'autor' => 'Ray Bradbury',
                'materia' => NULL,
                'edicao' => '2',
                'tipo' => 'paradidatico',
                'created_at' => '2018-07-11 14:34:55',
                'updated_at' => '2018-07-11 14:34:55',
            ),
            4 => 
            array (
                'id' => 5,
                'id_usuario' => 1,
                'titulo' => 'Mindset',
                'autor' => 'Carol Dweck',
                'materia' => NULL,
                'edicao' => '1',
                'tipo' => 'paradidatico',
                'created_at' => '2018-07-11 14:35:29',
                'updated_at' => '2018-07-11 14:35:29',
            ),
            5 => 
            array (
                'id' => 6,
                'id_usuario' => 1,
                'titulo' => 'Por que Fazemos o que Fazemos?',
                'autor' => 'Mario Sergio Cortella',
                'materia' => NULL,
                'edicao' => '1',
                'tipo' => 'paradidatico',
                'created_at' => '2018-07-11 14:36:05',
                'updated_at' => '2018-07-11 14:36:05',
            ),
        ));
        
        
    }
}