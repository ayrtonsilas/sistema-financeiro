@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Parcela</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <form method="post" action="{{ action('ParcelaController@salvar') }}">
            {{ csrf_field() }}
                <input type="hidden" name="id" value="{{old('id',$parcela->parcela_id)}}">
                <input type="hidden" name="pai" value="{{old('pai',$parcela->pai)}}">
                <input type="hidden" name="pailocal" value="{{old('pai',$parcela->pailocal)}}">
                <div class="form-group">
                    <label for="titulo">Data Vencimento:</label>
                    <input name="data_vencimento" type="text" class="form-control" id="data_vencimento" value="{{old('data_vencimento',$parcela->data_vencimento)}}">
                </div>
                <div class="form-group">
                    <label for="titulo">Valor:</label>
                    <input name="valor" type="text" class="form-control" id="valor" value="{{old('valor',$parcela->valor)}}">
                </div>
                <div class="form-group">
                    <label for="titulo">Valor Quitado:</label>
                    <input name="valor_quitado" type="text" class="form-control" id="valor_quitado" value="{{old('valor_quitado',$parcela->valor_quitado)}}">
                </div>
                <div class="form-group">
                    <label for="titulo">Data Quitacao:</label>
                    <input name="data_quitacao" type="text" class="form-control" id="data_quitacao" value="{{old('data_quitacao',$parcela->data_quitacao)}}">
                </div>
                <a href="{{ URL::previous() }}" class="btn btn-primary">Voltar</a>
                <button type="submit" class="btn btn-success">Salvar</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                    Excluir
                </button>  
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Excluir Parcela</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Tem certeza que deseja excluir a parcela</b>?
        </div>
        <div class="modal-footer">
            <form action="{{ action('ParcelaController@excluir') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{old('id',$parcela->parcela_id)}}">
                <input type="hidden" name="pai" value="{{old('pai',$parcela->pai)}}">
                <input type="hidden" name="pailocal" value="{{old('pai',$parcela->pailocal)}}">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button class="btn btn-danger" type="submit" data-toggle="modal" data-target="#confirmDelete">
                    Excluir
                </button>
            </form>
        </div>
        </div>
    </div>
</div>
@endsection