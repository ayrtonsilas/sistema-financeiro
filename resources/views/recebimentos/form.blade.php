@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Recebimento</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <form method="post" action="{{ action('RecebimentoController@salvar') }}">
            {{ csrf_field() }}
                <input type="hidden" name="id" value="{{old('id',$recebimento->id)}}">
                <div class="form-group">
                    <label for="titulo">Inclusão:</label>
                    <input name="data_inclusao" readonly type="text" class="form-control" id="titulo" value="{{old('data_inclusao',$recebimento->data_inclusao)}}">
                </div>
                <div class="form-group">
                    <label for="titulo">Observações:</label>
                    <input name="observacoes" type="text" class="form-control" id="titulo" value="{{old('observacoes',$recebimento->observacoes)}}">
                </div>
                <div class="form-group">
                    <label for="tipo">Tipos:</label>
                    <select required class="form-control" name="tipo_id" id="tipo">
                        <option value="">Selecione</option>
                        @foreach ($tipos as $t)
                            <option value="{{$t->id}}" @if($recebimento->tipo_id == $t->id) selected @endif }}>{{$t->descricao}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="tipo">Clientes:</label>
                    <select required class="form-control" name="cliente_id" id="tipo">
                        <option value="">Selecione</option>
                        @foreach ($clientes as $c)
                            <option value="{{$c->id}}" @if($recebimento->cliente_id == $c->id) selected @endif }}>{{$c->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="tipo">Desconto:</label>
                    <select required class="form-control" name="desconto_id" id="tipo">
                        <option value="">Selecione</option>
                        @foreach ($descontos as $c)
                            <option value="{{$c->id}}" @if($recebimento->desconto_id == $c->id) selected @endif }}>{{$c->valor}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="tipo">Operador:</label>
                    <select required class="form-control" name="operador_id" id="tipo">
                        <option value="">Selecione</option>
                        @foreach ($operadores as $c)
                            <option value="{{$c->id}}" @if($recebimento->operador_id == $c->id) selected @endif }}>{{$c->nome}}</option>
                        @endforeach
                    </select>
                </div>




                <div class="form-group">
                <a class="btn btn-success" href="{{ action('ParcelaController@parcela',['tela' =>'r','acao' => 'novo', 'id' => $recebimento->id]) }}">
                        Adicionar Parcela
                    </a>
                </div>

                <table style="border:1px solid #000;" class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Valor</th>
                            <th scope="col">Valor Quitado</th>
                            <th scope="col">Vencimento</th>
                            <th scope="col">Quitação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($parcelas as $r)
                        <tr>
                            <td>
                                <a href="{{ action('ParcelaController@parcela',['tela' =>'r','acao' => 'editar', 'id' => $r->parcela_id]) }}"> {{$r->valor}}</a>
                            </td>
                            <td>
                                <a href="{{ action('ParcelaController@parcela',['tela' =>'r','acao' => 'editar', 'id' => $r->parcela_id]) }}">{{$r->valor_quitado}}</a>
                            </td>
                            <td>
                                <a href="{{ action('ParcelaController@parcela',['tela' =>'r','acao' => 'editar', 'id' => $r->parcela_id]) }}">{{$r->data_vencimento}}</a>
                            </td>
                            <td>
                                <a href="{{ action('ParcelaController@parcela',['tela' =>'r','acao' => 'editar', 'id' => $r->parcela_id]) }}">{{$r->data_quitacao}}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>


                <a href="{{ URL::previous() }}" class="btn btn-primary">Voltar</a>
                <button type="submit" class="btn btn-success">Salvar</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                    Excluir
                </button>
                
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Tem certeza que deseja excluir o recebimento <b>{{$recebimento->titulo}}</b>?
        </div>
        <div class="modal-footer">
            <form action="{{ action('RecebimentoController@excluir',['recebimento' => $recebimento->id]) }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button class="btn btn-danger" type="submit" data-toggle="modal" data-target="#confirmDelete">
                    Excluir
                </button>
            </form>
        </div>
        </div>
    </div>
</div>
@endsection