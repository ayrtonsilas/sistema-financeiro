@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        
        <div class="col-sm-6">
            <h1>Recebimentos</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-12">
            
            @if (\Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show">
                    <ul>
                        <li>{!! \Session::get('error') !!}</li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <a href="{{action('RecebimentoController@novo')}}" class="btn btn-success">Novo</a><br><br>
            <table style="border:1px solid #000;" class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Data Inclusão</th>
                        <th scope="col">Desconto</th>
                        <th scope="col">Observações</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Cliente</th>
                        <th scope="col">CPF</th>
                        <th scope="col">Operador</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($recebimentos as $r)
                    <tr>
                        <th scope="row">
                            <a href="{{ action('RecebimentoController@editar',['recebimento' => $r->id]) }}">{{$r->id}}</a>
                        </th>
                        <td>
                            <a href="{{ action('RecebimentoController@editar',['recebimento' => $r->id]) }}">{{$r->data_inclusao}}</a>
                        </td>
                        <td>
                            <a href="{{ action('RecebimentoController@editar',['recebimento' => $r->id]) }}">{{$r->desconto}}</a>
                        </td>
                        <td>
                            <a href="{{ action('RecebimentoController@editar',['recebimento' => $r->id]) }}">{{$r->observacoes}}</a>
                        </td>
                        <td>
                            <a href="{{ action('RecebimentoController@editar',['recebimento' => $r->id]) }}">{{$r->tipodesc}}</a>
                        </td>
                        <td>
                            <a href="{{ action('RecebimentoController@editar',['recebimento' => $r->id]) }}">{{$r->cliente}}</a>
                        </td>
                        <td>
                            <a href="{{ action('RecebimentoController@editar',['recebimento' => $r->id]) }}">{{$r->cpf}}</a>
                        </td>
                        <td>
                            <a href="{{ action('RecebimentoController@editar',['recebimento' => $r->id]) }}">{{$r->operador}}</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection