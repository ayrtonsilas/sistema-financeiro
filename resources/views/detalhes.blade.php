@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <div class="col-lg-3">
          <div class="list-group">
            <a href="/paradidaticos" class="list-group-item">Paradidático</a>
            <a href="#collapseExample" class="list-group-item" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample">Didático</a>
            <span class="collapse" id="collapseExample">
              <div class="list-group">
                <a href="/didaticos/portugues" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>Português</a>
                <a href="/didaticos/matematica" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>Matemática</a>
                <a href="/didaticos/literatura" class="list-group-item"><i class="fas fa-fw fa-arrow-circle-right"></i>Literatura</a>
              </div>
            </span>
          </div>
          <div class="row">
            <div class="col-sm-12">
            </div>
          </div>
          <br>
          <form action="{{ action('HomeController@pesquisar') }}" method="POST">
            <div class="form-group">
            {{ csrf_field() }}
              <label for="buscar">Busque por autor, matéria ou título</label>
              <input id="buscar" placeholder="Buscar no site..." class="form-control" type="search" name="buscar">
            </div>
            <input type="submit" value="Buscar" class="btn btn-success">
          </form>
        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">
            <div class="row justify-content-center text-center">
                <div class="col-sm-12">
                    <h1>
                        {{$anuncio->nome}}
                    </h1>
                    <hr>
                </div>
                <div class="col-sm-6">
                <img  class="col-sm-12" src="{{ asset($anuncio->imagem? 'images/' . $anuncio->imagem:'img/default-image.jpg') }}">
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>{{$anuncio->livro->titulo}}</h3>
                        </div>
                        <div class="col-sm-12 text-left">
                            <b>Descrição:</b> {{$anuncio->descricao}}
                        </div>
                        @if ($anuncio->livro->tipo == 'didatico')
                        <div class="col-sm-12 text-left">
                            <b>Matéria:</b> {{$anuncio->materia}}
                        </div>
                        @endif
                        <div class="col-sm-12 text-left">
                            <b>Autor:</b> {{$anuncio->livro->autor}}
                        </div>
                        <div class="col-sm-12 text-left">
                            <b>Edição:</b> {{$anuncio->livro->edicao}}
                        </div>
                        <div class="col-sm-12 text-left">
                            <b>Estado:</b> {{$anuncio->estado}}
                        </div>

                        <div class="col-sm-12 text-left">
                            <b>Publicado por:</b> {{$anuncio->user->name}}
                        </div>
                        
                        <div class="col-sm-12 text-right">
                        <hr>
                            <b>Preço: </b>${{$anuncio->preco}}
                        </div>
                        <div class="col-sm-12 text-center">
                            @if (Auth::guest())
                                <a href="/login" class="btn btn-success">Tenho interesse</a>
                              @else 
                                <form action="{{ action('InteresseController@salvar')}}" method="POST">
                                  {{ csrf_field() }}
                                  <input required name="id_anuncio" type="hidden" id="id_anuncio" value="{{$anuncio->id}}">
                                  <input required name="id_usuario" type="hidden" id="id_usuario" value="{{Auth::user()->id}}">
                                  <input type="submit" value="Tenho interesse" class="btn btn-success">
                                </form>
                                
                              @endif
                        </div>
                        
                    </div>
                </div>
            </div>
          <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
@endsection
